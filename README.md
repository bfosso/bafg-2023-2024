# ESERCITAZIONI PRATICHE DEL CORSO DI BIOINFORMATICA ED ANALISI FUNZIONALE DEL GENOMA   

## Programma
### Esercitazione 1 (09-16/11/2023)
#### Banche dati: definizioni generali e sistemi di retrieval
- [X] [Introduzione alla Bioinformatica](Intro_Bioinfo.md)
- [X] [Definizione di Database e struttura delle entry **GenBank**](Banche_dati/banche_dati.md)  
- [x] [Sistemi di retrieval](Banche_dati/retrieval.md)  
- [X] [PubMed](Banche_dati/pubmed.md)


### Esercitazione 2 (23-30/11/2023)
#### Genome Browser 
- [X] [ENSEMBL](Genome_Browser/Ensembl.md)
- [X] [UCSC](Genome_Browser/UCSC.md)


### Esercitazione 3 (5-7/12/2023)
#### Allineamento tra sequenze
- [X] [Allineamento](Allineamento_tra_sequenze/allineamento.md)
- [X] [Utilizzo di BLAST e BLAT per l'analisi genomica](Allineamento_tra_sequenze/blast_genomico.md)
- [X] [Connessione al server e introduzione al BASH](BASH/Accesso_al_server.md)  

### Esercitazione 4 (13/12/2023 - 08/01/2024)
### Genome Assembly
- [X] [Connessione al server e introduzione al BASH (seconda parte)](BASH/Accesso_al_server.md#il-formato-fasta)
- [X] [Genome Assembly](Genome_Assembly/genome_assembly.md)

### Esercitazione 5 (12-15/01/2024)
- [X] [Genome Assembly](Genome_Assembly/genome_assembly.md#scopo-dellesercitazione-1)


### Esercitazione 6 (17-19/01/2024)
- [X] [DNA Metabarcoding](Metabarcoding/metabarcoding.md)

## Relazioni
[Relazioni ](relazioni.md)
